#!/usr/bin/python3
#
# SPDX-FileCopyrightText: Peter Pentchev <roam@ringlet.net>
# SPDX-License-Identifier: BSD-2-Clause

"""Setup definitions for trivval, the trivial validation helper."""

from __future__ import annotations

import setuptools  # type: ignore[import]


setuptools.setup()
